import React from "react";
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Home from "../src/components/home";
import TeacherDetail from "./components/teacherDetail";

const App = () => {

  return (
    <Router>
      <div>
        <Route path="/" component={Home} exact />
        <Route path="/teacher-info/:id" component={TeacherDetail} exact />
      </div>
    </Router>
  )
}

export default App;