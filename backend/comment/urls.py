
from django.urls import path
from . import views 

urlpatterns = [
    
    path('api/v1/comment/', views.CommentListCreateView.as_view()),
    path('api/v1/comment/<int:pk>/', views.CommentDetailUpdateDeleteView.as_view()),
    
    
]
